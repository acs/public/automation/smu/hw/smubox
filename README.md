# **smuBOX** <br/> _Plastic enclosure with DIN Rail support_

smuBOX is a 3D printable plastic case designed to install the smuHAT in a standard electrical cabinet over DIN rail.
The side entrance allows a very simple mounting style, avoiding mechanical stress of the board.
A transparent window provides a direct view of the smuHAT board and of the status indicators.

## Parts
The enclosure was [initially](https://www.tinkercad.com/things/98t7FUB94Sx) designed using the free, online 3D modeling program [**Tinkercad**](http://www.tinkercad.com/).
The DIN rail hooks were later added using [**FreeCAD**](https://www.freecadweb.org/) and the final assembled parts were exported in STEP format.
If not required, the DIN rail hooks can be removed from the STEP file before manufacturing.

[<img src="docs/smu_exploded.png"  width="944" height="500">](docs/smu_exploded.png)

[<img src="docs/smuBOX_case.png"  width="125" height="100">](docs/smuBOX_case.png)
&nbsp;
[<img src="docs/smuBOX_cover.png"  width="135" height="100">](docs/smuBOX_cover.png)
&nbsp;
[<img src="docs/smuBOX_full.png"  width="120" height="100">](docs/smuBOX_full.png)

## Media
[<img src="docs/smu.png"  width="194" height="100">](docs/smu.png)

## Copyright
2017-2021, Carlo Guarnieri (ACS) <br/>
2021, Institute for Automation of Complex Power Systems, EONERC

## License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

## Funding
<a rel="funding" href="https://hyperride.eu/"><img alt="HYPERRIDE" style="border-width:0" src="docs/hyperride_logo.png" height="63"/></a>&nbsp;
<a rel="funding" href="https://cordis.europa.eu/project/id/957788"><img alt="H2020" style="border-width:0" src="https://hyperride.eu/wp-content/uploads/2020/10/europa_flag_low.jpg" height="63"/></a><br />
This work was supported by <a rel="Hyperride" href="https://hyperride.eu/">HYbrid Provision of Energy based on Reliability and Resiliency via Integration of DC Equipment</a> (HYPERRIDE), a project funded by the European Union's Horizon 2020 research and innovation programme under <a rel="H2020" href="https://cordis.europa.eu/project/id/957788"> grant agreement No. 957788</a>.

## Contact

[![EONERC ACS Logo](docs/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- [Carlo Guarnieri Calò Carducci, Ph.D.](mailto:carlo.guarnieri@ieee.org)

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[E.ON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)